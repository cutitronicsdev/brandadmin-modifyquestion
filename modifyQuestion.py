import traceback
import logging
import pymysql
import json
import uuid
import sys
import os

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def openConnection():
    """Generic MySQL DB connection handler"""
    global Connection
    try:
        Connection = pymysql.connect(os.environ['v_db_host'], os.environ['v_username'], os.environ['v_password'], os.environ['v_database'], connect_timeout=5)
    except Exception as e:
        logger.error(e)
        logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
        raise e


def fn_checkBrandExist(lv_CutitronicsBrandID):

    lv_list = ""
    lv_statement = "SELECT CutitronicsBrandID, BrandName, ContactName, ContactTitle, ContactEmail FROM BrandDetails WHERE CutitronicsBrandID = %(CutitronicsBrandID)s"
    try:
        with Connection.cursor() as cursor:
            cursor.execute(lv_statement, {'CutitronicsBrandID': lv_CutitronicsBrandID})
            
            for row in cursor:
                field_names = [i[0] for i in cursor.description]
                lv_list = dict(zip(field_names, row))

            if len(lv_list) == 0:
                return(400)
            else:
                return lv_list

    except pymysql.err.IntegrityError as e:
        logging.info(e.args)
        return 500


def fn_checkQuestionExists(lv_CutitronicsBrandID, lv_QuestionKey):

    lv_list = ""
    lv_statement = "SELECT QuestionKey, QuestionSet, CutitronicsBrandID, QuestionText FROM ClientSurveyQuestion WHERE CutitronicsBrandID = %(CutitronicsBrandID)s AND QuestionKey = %(QuestionKey)s"

    try:
        with Connection.cursor() as cursor:
            cursor.execute(lv_statement, {'CutitronicsBrandID': lv_CutitronicsBrandID, 'QuestionKey': lv_QuestionKey})

            for row in cursor:
                field_names = [i[0] for i in cursor.description]
                lv_list = dict(zip(field_names, row))
            
            if len(lv_list) == 0:
                return 400
            else:
                return lv_list
    except pymysql.err.IntegrityError as e:
        logger.error(e.args)
        return 500


def fn_updateQuestion(lv_CutitronicsBrandID, lv_QuestionKey, lv_QuestionText):

    lv_statement = "UPDATE ClientSurveyQuestion SET QuestionText = %(QuestionText)s WHERE CutitronicsBrandID = %(CutitronicsBrandID)s AND QuestionKey = %(QuestionKey)s"

    try:
        with Connection.cursor() as cursor:
            cursor.execute(lv_statement, {'CutitronicsBrandID': lv_CutitronicsBrandID, 'QuestionKey': lv_QuestionKey, 'QuestionText': lv_QuestionText})

            if cursor.rowcount == 1:
                return 200
            elif cursor.rowcount > 1:
                logger.info("More than one row affect")
                return 400
            elif cursor.rowcount == 0:
                logger.info("No update")
                return 400
    except pymysql.err.IntegrityError as e:
        logger.error(e.args)
        return 500


def lambda_handler(event, context):
    """
    modifyQuestion

    Modifies the question text of a brands client survey question
    """
    openConnection()

    logger.info("Lambda function - {function_name}".format(function_name=context.function_name))

    # Return block

    modifyQuestion_out = {"Service": context.function_name, "Status": "Success", "ErrorCode": "", "ErrorDesc": "", "ErrorStack": "", "ServiceOutput": {}}

    #Variables
    lv_CutitronicsBrandID = None
    lv_QuestionKey = None
    lv_QuestionText = None

    try:
        try:
            body = json.loads(event['body'])
        except KeyError as e:
            try:
                body = event
            except TypeError:
                app_json = json.dumps(event)
                event = {"body": app_json}
                body = json.loads(event['body'])

        logger.info("Payload body - '{body}'".format(body=body))

        lv_CutitronicsBrandID = body.get('CutitronicsBrandID')
        lv_QuestionKey = body.get('QuestionKey')
        lv_QuestionText = body.get('QuestionText')

        """
        1. Does Brand Exist
        """

        lv_BrandType = fn_checkBrandExist(lv_CutitronicsBrandID)

        if lv_BrandType == 400:

            logger.error("")
            logger.error("Could not find brand '{BrandID}' in the back office systems".format(BrandID=lv_CutitronicsBrandID))
            logger.error("")

            modifyQuestion_out['Status'] = "Error"
            modifyQuestion_out['ErrorCode'] = context.function_name + "_001"  # viewGuests_out_001
            modifyQuestion_out['ErrorDesc'] = "Supplied CutitronicsBrandID does not exist in the BO Database"
            modifyQuestion_out['ServiceOutput']['CutitronicsBrandID'] = body.get('CutitronicsBrandID')
            modifyQuestion_out['ServiceOutput']['QuestionKey'] = body.get('QuestionKey')
            modifyQuestion_out['ServiceOutput']['QuestionText'] = body.get('QuestionText')

            Connection.close()

            # Lambda response

            return {
                "isBase64Encoded": "false",
                "statusCode": 400,
                "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
                "body": json.dumps(modifyQuestion_out)
            }
        
        logger.info("")
        logger.info("Found brand '{BrandID}' in the back office".format(BrandID=lv_CutitronicsBrandID))
        logger.info("")

        '''
        2. Does Question Exist?
        '''

        lv_QuestionType = fn_checkQuestionExists(lv_CutitronicsBrandID, lv_QuestionKey)

        if lv_QuestionType == 400:

            logger.error("")
            logger.error("Could not find question '{QKey}' for brand '{BrandID}' in the back office".format(QKey=lv_QuestionKey, BrandID=lv_CutitronicsBrandID))
            logger.error("")

            modifyQuestion_out['Status'] = "Error"
            modifyQuestion_out['ErrorCode'] = context.function_name + "_001"  # viewGuests_out_001
            modifyQuestion_out['ErrorDesc'] = "Supplied CutitronicsBrandID does not exist in the BO Database"
            modifyQuestion_out['ServiceOutput']['CutitronicsBrandID'] = body.get('CutitronicsBrandID')
            modifyQuestion_out['ServiceOutput']['QuestionKey'] = body.get('QuestionKey')
            modifyQuestion_out['ServiceOutput']['QuestionText'] = body.get('QuestionText')

            Connection.close()

            # Lambda response

            return {
                "isBase64Encoded": "false",
                "statusCode": 400,
                "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
                "body": json.dumps(modifyQuestion_out)
            }
        

        logger.info("")
        logger.info("Found question '{QKey}' for brand '{BrandID}' in the back office".format(QKey=lv_QuestionKey, BrandID=lv_CutitronicsBrandID))
        logger.info("")

        '''
        3. Update Question Text
        '''

        lv_UpdateType = fn_updateQuestion(lv_CutitronicsBrandID, lv_QuestionKey, lv_QuestionText)

        if lv_UpdateType == 400:

            logger.error("")
            logger.error("Failed to update question '{QKey}' with new text '{QText}'".format(QKey=lv_QuestionKey, QText=lv_QuestionText))
            logger.error("")

            modifyQuestion_out['Status'] = "Error"
            modifyQuestion_out['ErrorCode'] = context.function_name + "_001"  # viewGuests_out_001
            modifyQuestion_out['ErrorDesc'] = "Supplied CutitronicsBrandID does not exist in the BO Database"
            modifyQuestion_out['ServiceOutput']['CutitronicsBrandID'] = body.get('CutitronicsBrandID')
            modifyQuestion_out['ServiceOutput']['QuestionKey'] = body.get('QuestionKey')
            modifyQuestion_out['ServiceOutput']['QuestionText'] = body.get('QuestionText')

            Connection.rollback()
            Connection.close()

            return {
                "isBase64Encoded": "false",
                "statusCode": 400,
                "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
                "body": json.dumps(modifyQuestion_out)
            }


        logger.info("")
        logger.info("Successfully updated question '{QKey}' with new text '{QText}' in the back office".format(QKey=lv_QuestionKey, QText=lv_QuestionText))
        logger.info("")

        # Commit and close connection
        Connection.commit()
        Connection.close()

        # Build Output
        modifyQuestion_out['Status'] = "Success"
        modifyQuestion_out['ErrorCode'] = ""  # viewGuests_out_001
        modifyQuestion_out['ErrorDesc'] = ""
        modifyQuestion_out['ServiceOutput']['CutitronicsBrandID'] = body.get('CutitronicsBrandID')
        modifyQuestion_out['ServiceOutput']['QuestionKey'] = body.get('QuestionKey')
        modifyQuestion_out['ServiceOutput']['QuestionText'] = body.get('QuestionText')

        return {
            "isBase64Encoded": "false",
            "statusCode": 200,
            "headers": {"x-custom-header": context.function_name},
            "body": json.dumps(modifyQuestion_out)
        }


    except Exception as e:
        logger.error("CATCH-ALL ERROR: Unexpected error: '{error}'".format(error=e))

        exception_type, exception_value, exception_traceback = sys.exc_info()
        traceback_string = traceback.format_exception(exception_type, exception_value, exception_traceback)
        err_msg = json.dumps({
            "errorType": exception_type.__name__,
            "errorMessage": str(exception_value),
            "stackTrace": traceback_string
        })
        logger.error(err_msg)

        Connection.rollback()
        Connection.close()

        # Populate Cutitronics reply block

        modifyQuestion_out['Status'] = "Error"
        modifyQuestion_out['ErrorCode'] = context.function_name + "_000"  # CreateTherapistSession_000
        modifyQuestion_out['ErrorDesc'] = "CatchALL"
        modifyQuestion_out["ServiceOutput"]["CutitronicsBrandID"] = body.get('CutitronicsBrandID', "")

        # Lambda response

        return {
            "isBase64Encoded": "false",
            "statusCode": 500,
            "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
            "body": json.dumps(modifyQuestion_out)
        }

# class context:
#     def __init__(self):
#         self.function_name = "modifyQuestion"

# context = context()
# event = {
#     "CutitronicsBrandID": "0003",
#     "QuestionKey": "vclarins1",
#     "QuestionText": "! Updated Question !"
# }

# x = lambda_handler(event, context)
# print(x)